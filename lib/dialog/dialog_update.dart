import 'package:flutter/material.dart';
import 'package:learn_flutter/utils/utils.dart';

import '../models/infor_model.dart';
import '../widgets/text_input.dart';
class DialogUdate extends StatefulWidget {
  const DialogUdate({Key? key, required this.item, required this.list, required this.stateList}) : super(key: key);
  final InforModel item;
  final  List<InforModel> list;
  final Function stateList;

  @override
  State<DialogUdate> createState() => _DialogUdateState();
}

class _DialogUdateState extends State<DialogUdate> {
  late TextEditingController textNameController;
  late TextEditingController textPassController;
  String dropdownValue =  'Male';
  @override
  void initState() {
    textNameController = TextEditingController(text:widget.item.name);
    textPassController = TextEditingController(text: widget.item.pass);
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: AlertDialog(
        title: const Center(child: Text('EDIT')),
        content: SizedBox(
          child: Column(
            children: [
              TextInput(
                title: "Full Name",
                required: true,
                hinText: "Enter your name",
                textController: textNameController,
                check: false,
              ),

          Padding(
            padding: const EdgeInsets.fromLTRB(30,10,30,10),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.centerLeft,
                  child: const Text('Sex'),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  height: 49,
                  padding: const EdgeInsets.only(
                      left: 16, right: 16),
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.grey,
                          width: 1),
                      borderRadius:
                      BorderRadius.circular(
                          5),
                      color: Colors.white),
                  child: DropdownButton<String>(
                    value: dropdownValue,
                    isExpanded: true,
                    icon: const Icon(Icons
                        .keyboard_arrow_down),
                    elevation: 16,
                    underline: Container(
                      height: 2,
                    ),
                    onChanged: (String? value) {
                      setState(() {
                       dropdownValue = value!;
                      });
                    },
                    items: Utils().list2.map<
                        DropdownMenuItem<
                            String>>(
                            (String value) {
                          return DropdownMenuItem<
                              String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                  ),
                ),
              ],
            ),
          ),

              TextInput(
                title: "Citizen ID",
                hinText: "",
                textController: textPassController,
                check: true,
              ),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 100,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.grey),
                      child: const Text('cancel'),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  SizedBox(
                    width: 100,
                    child: ElevatedButton(
                      onPressed: () {
                        String name2 = textNameController.text.trim();
                        String pass2 = textPassController.text.trim();
                        String sex = dropdownValue;
                        widget.item.name = name2 ;
                        widget.item.pass = pass2;
                        widget.item.sex = sex;
                        if(widget.list ==  name2 && widget.list == pass2 ){
                          setState(() {
                            widget.list.add(InforModel(name2, sex, pass2));
                          });
                        }
                        widget.stateList(widget.list);

                        Navigator.of(context).pop();
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const Text(
                        'Save',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );;
  }
}
