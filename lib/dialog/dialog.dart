import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:learn_flutter/dialog/dialog_update.dart';
import '../models/infor_model.dart';
class DialogCustom extends StatefulWidget {
  const DialogCustom({Key? key, required this.item, required this.list, required this.stateList}) : super(key: key);
  final InforModel item;
  final  List<InforModel> list;
  final Function stateList;

  @override
  State<DialogCustom> createState() => _DialogState();
}

class _DialogState extends State<DialogCustom> {

  @override
  Widget build(BuildContext context) {
    return CupertinoActionSheet(
      title: const Text('choose Action'),
      cancelButton: CupertinoActionSheetAction(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: const Text('Cancel'),
      ),
      actions: [
        CupertinoActionSheetAction(
            onPressed: () {
              Navigator.of(context).pop();
              showDialog(
                context: context,
                builder: (context) {
                  return DialogUdate(list: widget.list,item: widget.item,stateList: widget.stateList,);
                },
              );
            },
            child: const Text('Edit')),
        CupertinoActionSheetAction(
          onPressed: () {
            widget.list.remove(widget.item);
            widget.stateList(widget.list);
            Navigator.of(context).pop();
          },
          child: const Text(
            'Delete',
            style: TextStyle(color: Colors.red),
          ),
        )
      ],
    );
  }
}
