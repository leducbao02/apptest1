import 'package:flutter/material.dart';
import 'package:learn_flutter/models/infor_model.dart';
import 'package:learn_flutter/utils/utils.dart';
import 'package:learn_flutter/widgets/row.dart';
import '../widgets/text_input.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<InforModel> list = [];
  late TextEditingController textNameController;
  late TextEditingController textPassController;
  String dropdownValue =  'Male';

  stateList(List<InforModel> _list){
       setState(() {
          list = _list;
       });
  }
  @override
  void initState() {
    textNameController = TextEditingController();
    textPassController = TextEditingController();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: SingleChildScrollView(
        child: Container(
            alignment: Alignment.topCenter,
            margin: const EdgeInsets.fromLTRB(0, 30, 0, 0),
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Column(
                  children: [
                    GestureDetector(
                      onTap: (){
                        String name = textNameController.text.trim();
                        String pass = textPassController.text.trim();
                      Utils.validate(name,pass,const Scaffold(),context);
                        if (name.isNotEmpty && pass.isNotEmpty) {
                          setState(() {
                            textNameController.text = "";
                            textPassController.text = "";
                            list.add(InforModel(name, dropdownValue , pass));
                          });
                        }
                      },
                      child: Container(
                        margin: const EdgeInsets.fromLTRB(250, 0, 0, 0),
                        height: 40,
                            width: 100,

                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: const Color(0xffe7e7e7),
                        ),

                        child: const Center(
                          child: Text(
                            'Thêm',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,fontSize: 17),
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        TextInput(
                          title: "Full Name",
                          required: true,
                          hinText: "Enter your name",
                          textController: textNameController, check: false,
                        ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30,10,30,10),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text('Sex'),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 49,
                    padding: const EdgeInsets.only(
                        left: 16, right: 16),
                    decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.grey,
                            width: 1),
                        borderRadius:
                        BorderRadius.circular(
                            5),
                        color: Colors.white),
                    child: DropdownButton<String>(
                      value: dropdownValue,
                      isExpanded: true,
                      icon: const Icon(Icons
                          .keyboard_arrow_down),
                      elevation: 16,
                      underline: Container(
                        height: 2,
                      ),
                      onChanged: (String? value) {
                        setState(() {
                          dropdownValue = value!;
                        });
                      },
                      items: Utils().list2.map<
                          DropdownMenuItem<
                              String>>(
                              (String value) {
                            return DropdownMenuItem<
                                String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                    ),
                  ),
                ],
              ),
            ),
                        TextInput(
                          title: "Citizen ID",
                          hinText: "",
                          textController: textPassController,
                          check: true,
                          CheckInput: true,
                          keyboardType: true,
                        ),
                      ],
                    ),
                    const Divider(),
                  ],
                ),
                SizedBox(
                  child: ListView.builder(
                      itemCount: list.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) => RowItem( item: list[index],list: list,stateLils: stateList,)),
                )
              ],
            )),
      )),
    );
  }

}
