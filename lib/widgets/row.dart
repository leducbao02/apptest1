import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:learn_flutter/dialog/dialog.dart';
import '../models/infor_model.dart';
class RowItem extends StatefulWidget {
  const RowItem({Key? key, required this.item, required this.list, required this.stateLils,}) : super(key: key);
  final InforModel item;
  final  List<InforModel> list;
  final Function stateLils;
  @override
  State<RowItem> createState() => _RowItemState();
}

class _RowItemState extends State<RowItem> {
  late TextEditingController textNameController;
  late TextEditingController textPassController;


  showButton(BuildContext context) {
    return showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return  DialogCustom(item: widget.item,list: widget.list,stateList: widget.stateLils,);
        });
  }



  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showButton(context);
      },
      child: Card(
        child: ListTile(
          title: Column(
            children: [
              Padding(
                padding:const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.fromLTRB(3, 0, 0, 7),
                    child: SizedBox(
                      width: 200,
                      child: Text(
                        widget.item.name,
                        overflow:TextOverflow.ellipsis,
                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    )),
              ),
              Padding(
                padding:const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.fromLTRB(3, 0, 0, 7),
                    child: Text(widget.item.sex)),
              ),
              Padding(
                padding:const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.fromLTRB(3, 0, 0, 5),
                    child: Text(widget.item.pass, )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
