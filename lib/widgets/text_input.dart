import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextInput extends StatefulWidget {
  const TextInput(
      {Key? key,
      required this.title,
      this.required,
      required this.hinText,
      required this.textController,
      required this.check, this.CheckInput, this.keyboardType})
      : super(key: key);
  final String title;
  final bool? required;
  final String? hinText;
  final bool? check;
  final bool? CheckInput;
  final bool? keyboardType;
  final TextEditingController textController;

  @override
  State<TextInput> createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  bool _showPass = false;

  checkkey(){
    if(widget.keyboardType == true){
      return  TextInputType.number;
    }
  }

  showRequiredInput() {
    if (widget.required != null && widget.required == true) {
      return const Text(
        '*',
        style: TextStyle(color: Colors.red),
      );
    }

    return Container();
  }


  checkNumber(){
    if(widget.CheckInput == true){
      return <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
    ];
    }

  }

  showIconButton(){
    if(widget.check == true){
      return IconButton(
        icon: _showPass ? Icon(Icons.remove_red_eye) : Icon(Icons.remove_red_eye_outlined),
        onPressed: () {
          showPass();
        },
      );
    }
    // return Container();
  }


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(30, 10, 30, 10),
      child: Column(
        children: [
          Row(
            children: [
              Text(widget.title),
              const SizedBox(
                width: 3,
              ),
              showRequiredInput()
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          TextField(
            keyboardType: checkkey(),
            inputFormatters: checkNumber(),
            obscureText: _showPass,
            controller: widget.textController,
            decoration: InputDecoration(
                suffixIcon: showIconButton(),
                hintText: widget.hinText,
                fillColor: Colors.white,
                filled: true,
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(color: Colors.white))),
          ),
        ],
      ),
    );
  }

  void showPass() {
    setState(() {
      _showPass = !_showPass;
    });
  }
}
