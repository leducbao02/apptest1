import 'package:flutter/material.dart';
import 'package:learn_flutter/page/home_screen.dart';


void main() {
  runApp(const MaterialApp(
    debugShowCheckedModeBanner: false,
    home: HomeScreen(),
  ));
}


